module Rhizome
  %w(
    bounds
    coord
    motion
    lifecycle
    bud
    shoot
    flower
    node
    rhizome
  ).each { |f| require_relative "models/#{f}" }
end
