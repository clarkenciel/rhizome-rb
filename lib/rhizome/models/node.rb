module Rhizome
  class Node
    # rubocop:disable Naming/UncommunicativeMethodParamName
    def initialize left: Shoot.left,
                   up: Shoot.up,
                   right: Shoot.right,
                   down: Shoot.down
      @left = left
      @up = up
      @right = right
      @down = down
    end
    # rubocop:enable Naming/UncommunicativeMethodParamName

    def buds
      [@left, @up, @right, @down]
    end

    def shoots
      buds.reject(&:flowered?)
    end

    def flowers
      buds.select(&:flowered?)
    end
  end
end
