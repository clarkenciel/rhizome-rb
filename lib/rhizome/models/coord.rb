module Rhizome
  class Coord
    attr_reader :x, :y
    # rubocop:disable Naming/UncommunicativeMethodParamName
    def initialize x, y
      @x = x
      @y = y
    end
    # rubocop:enable Naming/UncommunicativeMethodParamName

    def self.anywhere_in bounds
      new bounds.x_component, bounds.y_component
    end

    def self.origin
      new 0, 0
    end

    def distance other
      (other.x - x).abs + (other.y - y).abs
    end

    def move motion
      motion.call self
    end

    def in? bounds
      bounds.contains? self
    end
  end
end
