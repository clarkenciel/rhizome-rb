module Rhizome
  class Bud
    attr_reader :position
    def initialize position:
      @position = position
    end

    class << self
      %i(left up right down).each do |pos|
        define_method pos do |**args|
          new(position: pos, **args)
        end
      end
    end
  end
end
