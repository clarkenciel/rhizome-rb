module Rhizome
  module Motion
    # rubocop:disable Metrics/AbcSize
    def self.for label
      case label
      when :left  then ->(coord) { Coord.new coord.x.pred, coord.y }
      when :up    then ->(coord) { Coord.new coord.x, coord.y.succ }
      when :right then ->(coord) { Coord.new coord.x.succ, coord.y }
      when :down  then ->(coord) { Coord.new coord.x, coord.y.pred }
      else ->(coord) { coord }
      end
    end
    # rubocop:enable Metrics/AbcSize
  end
end
