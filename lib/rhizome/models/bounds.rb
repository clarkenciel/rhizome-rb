module Rhizome
  class Bounds
    def initialize x_range, y_range
      @x_range = x_range
      @y_range = y_range
    end

    def x_component
      @x_range.to_a.sample
    end

    def y_component
      @y_range.to_a.sample
    end

    def contains? coord
      @x_range.include?(coord.x) && @y_range.include?(coord.y)
    end
  end
end
