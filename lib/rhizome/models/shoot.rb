module Rhizome
  class Shoot < Bud
    def flowered?
      false
    end

    def bloom rhizome
      Flower.new position: @position, rhizome: rhizome
    end
  end
end
