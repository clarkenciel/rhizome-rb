module Rhizome
  class Lifecycle
    attr_reader :max_age, :age, :aging_rate
    def initialize max_age:, age: 0, aging_rate: rand
      @max_age = max_age
      @age = age
      @aging_rate = aging_rate
    end

    def self.living_until max_age
      new max_age: max_age
    end

    def alive?
      age < max_age
    end
  end
end
