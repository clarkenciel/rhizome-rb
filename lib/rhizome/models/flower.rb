module Rhizome
  class Flower < Bud
    attr_reader :rhizome
    def initialize rhizome:, **args
      super(**args)
      @rhizome = rhizome
    end

    def flowered?
      true
    end
  end
end
