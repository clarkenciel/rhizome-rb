module Rhizome
  class Rhizome
    attr_reader :value, :coord
    def initialize value:,
                   coord:,
                   dna:,
                   node: Node.new,
                   lifecycle: Lifecycle.living_until(100)
      @value = value
      @lifecycle = lifecycle
      @node = node
      @coord = coord
      @dna = dna
    end

    def alive?
      @lifecycle.alive?
    end

    def spawn?
      alive? && @node.shoots.any?
    end

    def new_growths
      @node.shoots.map do |shoot|
        Rhizome.new value: @dna.generate(@value),
          coord: coord.move(Motion.for shoot.position),
          dna: @dna
      end
    end

    def children
      @node.flowers.map(&:rhizome)
    end

    def sequences
      return [[@value]] unless children.any?
      children.flat_map { |r| r.sequences.map { |s| [@value, *s] } }
    end
  end
end
