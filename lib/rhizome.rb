module Rhizome
  %w(
    version
    models
  ).each { |f| require_relative "rhizome/#{f}" }
end
