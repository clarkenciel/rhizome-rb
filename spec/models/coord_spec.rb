RSpec.describe Rhizome::Coord do
  describe '.origin' do
    subject { described_class.origin }
    it { is_expected.to have_attributes(x: 0, y: 0) }
  end

  describe '.anywhere_in' do
    it 'should create a coordinate with x and y components' do
      (21 * 21).times do
        x_range, y_range = (-10..10), (-10..10)
        bounds = Rhizome::Bounds.new x_range, y_range
        instance = described_class.anywhere_in bounds

        expect(x_range).to include instance.x
        expect(y_range).to include instance.y
      end
    end
  end

  describe '#distance' do
    it 'should be the manhattan distance between the two coords' do
      100.times do
        coord_one, coord_two = Array.new(2) do
          described_class.anywhere_in Rhizome::Bounds.new(-100..100, -100..100)
        end

        expect(coord_one.distance coord_two).to eq(
          (coord_two.x - coord_one.x).abs + (coord_two.y - coord_one.y).abs
        )
      end
    end
  end

  describe '#in?' do
    subject { Rhizome::Coord.origin.in? bounds }

    context 'when the coord is inside of both the horizontal and vertical bounds' do
      let(:bounds) { Rhizome::Bounds.new (-10..10), (-10..10) }
      it { is_expected.to be_truthy }
    end

    context 'when the coord is to the left of the horizontal bounds' do
      let(:bounds) { Rhizome::Bounds.new (1..10), (-10..10) }
      it { is_expected.to be_falsey }
    end

    context 'when the coord is to the right of the horizontal bounds' do
      let(:bounds) { Rhizome::Bounds.new (-10..-1), (-10..10) }
      it { is_expected.to be_falsey }
    end

    context 'when the coord is above the vertical bounds' do
      let(:bounds) { Rhizome::Bounds.new (-10..10), (-10..-1) }
      it { is_expected.to be_falsey }
    end

    context 'when the coord is below the vertical bounds' do
      let(:bounds) { Rhizome::Bounds.new (-10..10), (10..10) }
      it { is_expected.to be_falsey }
    end
  end
end
