RSpec.describe Rhizome::Flower do
  describe '#flowered?' do
    subject { described_class.up(rhizome: instance_double(Rhizome::Rhizome)).flowered? }
    it { is_expected.to be_truthy }
  end
end
