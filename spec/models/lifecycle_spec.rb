RSpec.describe Rhizome::Lifecycle do
  describe '.living_until' do
    subject { described_class.living_until max_age }
    let(:max_age) { 200 }

    it { is_expected.to have_attributes age: 0 }
    it { is_expected.to have_attributes aging_rate: (be < 1.0).and(be > 0.0) }
    it 'should return a new lifecycle with a max age matching the specified age' do
      is_expected.to have_attributes max_age: max_age
    end
  end

  describe '#alive?' do
    subject { lifecycle.alive? }

    let(:lifecycle) do
      described_class.new age: age,
        max_age: 100,
        aging_rate: 1.0
    end

    context 'when the age is greater than the maximum age' do
      let(:age) { 101 }
      it { is_expected.to be_falsey }
    end

    context 'when the age is below the maximum age' do
      let(:age) { 10 }
      it { is_expected.to be_truthy }
    end
  end
end
