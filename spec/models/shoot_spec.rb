RSpec.describe Rhizome::Shoot do
  describe '#flowered?' do
    subject { described_class.up.flowered? }
    it { is_expected.to be_falsey }
  end

  describe '#bloom' do
    subject { described_class.up.bloom 'hi' }
    it do
      is_expected.to have_attributes(rhizome: 'hi', position: :up).
        and(be_flowered)
    end
  end
end
