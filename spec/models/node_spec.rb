RSpec.describe Rhizome::Node do
  describe '#buds' do
    subject { described_class.new.buds }

    it { is_expected.to be_an(Array).and have_attributes(size: 4) }

    context 'when the node is not instantiated with buds' do
      subject { described_class.new.buds }
      it { is_expected.to be_an(Array).and(have_attributes size: 4).and(exclude be_flowered) }
    end
  end

  describe '#shoots' do
    subject { described_class.new(left: shoot, up: flower).shoots }
    let(:shoot) { Rhizome::Shoot.left }
    let(:flower) { Rhizome::Flower.up rhizome: instance_double(Rhizome::Rhizome) }

    it { is_expected.to be_an(Array).and(have_attributes size: 3).and(exclude be_flowered) }
  end

  describe '#flowers' do
    subject { described_class.new(left: shoot, up: flower).flowers }
    let(:shoot) { Rhizome::Shoot.left }
    let(:flower) { Rhizome::Flower.up rhizome: instance_double(Rhizome::Rhizome) }

    it { is_expected.to be_an(Array).and(have_attributes size: 1).and all be_flowered }
  end
end
