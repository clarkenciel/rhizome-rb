RSpec.describe Rhizome::Rhizome do
  let(:instance) do
    described_class.new value: 'x',
      coord: coord,
      dna: dna,
      node: node,
      lifecycle: lifecycle
  end

  let(:lifecycle) { instance_double Rhizome::Lifecycle }
  let(:node) { instance_double Rhizome::Node }
  let(:dna) { double 'DNA' }
  let(:coord) { Rhizome::Coord.origin }

  describe '#alive?' do
    subject { instance.alive? }
    before { allow(lifecycle).to receive(:alive?).and_return true }
    it { is_expected.to eq lifecycle.alive? }
  end

  describe '#spawn?' do
    subject { instance.spawn? }

    before do
      allow(lifecycle).to receive(:alive?).and_return true
      allow(node).to receive(:shoots).and_return [{}]
    end

    it { is_expected.to be_truthy }

    context 'when the rhizome is not alive' do
      before { allow(lifecycle).to receive(:alive?).and_return false }
      it { is_expected.to be_falsey }
    end

    context 'when the rhizome has no available shoots' do
      before { allow(node).to receive(:shoots).and_return [] }
      it { is_expected.to be_falsey }
    end
  end

  describe '#new_growths' do
    subject { instance.new_growths }

    before { allow(dna).to receive(:generate).with instance.value }
    let(:node) { Rhizome::Node.new }

    it do
      is_expected.to have_attributes(size: node.shoots.size).
        and(all be_alive.and(satisfy { |r| r.coord.distance(coord) == 1 }))
    end
  end

  describe '#children' do
    subject { instance.children }

    let(:instance) do
      Rhizome::Rhizome.new value: 'root',
        coord: Rhizome::Coord.origin,
        dna: double('DNA'),
        node: node
    end

    context 'when the rhizome has children' do
      let(:child_one) do
        Rhizome::Rhizome.new value: 'child_one',
          dna: double('DNA'),
          coord: instance_double(Rhizome::Coord)
      end

      let(:child_two) do
        Rhizome::Rhizome.new value: 'child_two',
          dna: double('DNA'),
          coord: instance_double(Rhizome::Coord)
      end

      let(:node) do
        Rhizome::Node.new left: Rhizome::Flower.left(rhizome: child_one),
          up: Rhizome::Flower.up(rhizome: child_two)
      end

      it { is_expected.to include(child_one).and(include child_two) }
    end

    context 'when the rhizome has no children' do
      let(:node) { Rhizome::Node.new }
      it { is_expected.to be_empty }
    end
  end

  describe '#sequences' do
    subject { instance.sequences }

    context 'when the rhizome has children' do
      let(:child_one) do
        Rhizome::Rhizome.new value: 'child_one',
          dna: double('DNA'),
          coord: instance_double(Rhizome::Coord)
      end

      let(:child_two) do
        Rhizome::Rhizome.new value: 'child_two',
          dna: double('DNA'),
          coord: instance_double(Rhizome::Coord)
      end

      let(:instance) do
        Rhizome::Rhizome.new value: 'root',
          coord: Rhizome::Coord.origin,
          dna: double('DNA'),
          node: Rhizome::Node.new(left: Rhizome::Flower.left(rhizome: child_one),
                                  up: Rhizome::Flower.up(rhizome: child_two))
      end

      it { is_expected.to include(%w(root child_one)).and(include %w(root child_two)) }
    end

    context 'when the rhizome has no children' do
      let(:instance) do
        Rhizome::Rhizome.new value: 'root',
          coord: Rhizome::Coord.origin,
          dna: double('DNA'),
          node: Rhizome::Node.new
      end

      it { is_expected.to include(%w(root)) }
    end
  end
end
