RSpec.describe Rhizome::Bud do
  %i(left up right down).each do |position|
    describe position.to_s do
      subject { described_class.send position }
      it { is_expected.to have_attributes position: position }
    end
  end
end
