RSpec.describe Rhizome::Motion do
  describe '.for' do
    let(:coord) { Rhizome::Coord.origin }

    context 'left' do
      let(:motion) { described_class.for :left }
      it 'should return a motion that moves a coordinate left' do
        expect(motion.call coord).to have_attributes(x: coord.x - 1, y: coord.y)
      end
    end

    context 'right' do
      let(:motion) { described_class.for :right }
      it 'should return a motion that moves a coordinate right' do
        expect(motion.call coord).to have_attributes(x: coord.x + 1, y: coord.y)
      end
    end

    context 'up' do
      let(:motion) { described_class.for :up }
      it 'should return a motion that moves a coordinate up' do
        expect(motion.call coord).to have_attributes(x: coord.x, y: coord.y + 1)
      end
    end

    context 'down' do
      let(:motion) { described_class.for :down }
      it 'should return a motion that moves a coordinate down' do
        expect(motion.call coord).to have_attributes(x: coord.x, y: coord.y - 1)
      end
    end

    context 'when the motion is not known' do
      let(:motion) { described_class.for 'dogs' }
      it 'should return a motion that leaves a coordinate unmoved' do
        expect(motion.call coord).to have_attributes(x: coord.x, y: coord.y)
      end
    end
  end
end
