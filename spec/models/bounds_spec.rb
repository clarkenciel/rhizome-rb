RSpec.describe Rhizome::Bounds do
  describe '#x_component' do
    it 'generates a value inside of the specified x range' do
      100.times do
        x_range, y_range = (-10..10), (-10..10)
        bounds = described_class.new x_range, y_range
        expect(x_range).to include bounds.x_component
      end
    end
  end

  describe '#y_component' do
    it 'generates a value inside of the specified y range' do
      100.times do
        x_range, y_range = (-10..10), (-10..10)
        bounds = described_class.new x_range, y_range
        expect(y_range).to include bounds.y_component
      end
    end
  end

  describe '#contains?' do
    subject { Rhizome::Bounds.new((-10..10), (-10..10)).contains? coord }

    context 'when the coord is inside of both the horizontal and vertical bounds' do
      let(:coord) { Rhizome::Coord.new 0, 0 }
      it { is_expected.to be_truthy }
    end

    context 'when the coord is to the left of the horizontal bounds' do
      let(:coord) { Rhizome::Coord.new -20, 0 }
      it { is_expected.to be_falsey }
    end

    context 'when the coord is to the right of the horizontal bounds' do
      let(:coord) { Rhizome::Coord.new 20, 0 }
      it { is_expected.to be_falsey }
    end

    context 'when the coord is above the vertical bounds' do
      let(:coord) { Rhizome::Coord.new 0, -20 }
      it { is_expected.to be_falsey }
    end

    context 'when the coord is below the vertical bounds' do
      let(:coord) { Rhizome::Coord.new 0, 20 }
      it { is_expected.to be_falsey }
    end
  end
end
